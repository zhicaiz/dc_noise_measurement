EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title "DC Noise Measurement Board"
Date "2021-04-28"
Rev "v1"
Comp "LBNL"
Comment1 "Zhicai Zhang"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J1
U 1 1 609F7FF4
P 1450 1100
F 0 "J1" H 1500 1317 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 1500 1226 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x02_P2.54mm_Vertical" H 1450 1100 50  0001 C CNN
F 3 "~" H 1450 1100 50  0001 C CNN
	1    1450 1100
	1    0    0    -1  
$EndComp
Text Label 1100 1100 2    50   ~ 0
VIN0
Wire Wire Line
	1250 1100 1750 1100
Wire Wire Line
	1250 1200 1750 1200
Wire Wire Line
	1100 1100 1250 1100
Connection ~ 1250 1100
Connection ~ 1250 1200
Entry Wire Line
	10000 1300 10100 1400
Entry Wire Line
	10000 1500 10100 1600
Entry Wire Line
	10000 1700 10100 1800
Entry Wire Line
	10000 1900 10100 2000
Entry Wire Line
	10000 2100 10100 2200
Entry Wire Line
	10000 2300 10100 2400
Entry Wire Line
	10000 2500 10100 2600
Entry Wire Line
	10000 2700 10100 2800
Entry Wire Line
	10000 2900 10100 3000
Entry Wire Line
	10000 3100 10100 3200
Text HLabel 9950 3370 0    50   Output ~ 0
VIN[0..9]
Wire Bus Line
	9950 3370 10100 3370
Text Label 9890 1300 2    50   ~ 0
VIN0
Wire Wire Line
	10000 1300 9890 1300
Text Label 9890 1500 2    50   ~ 0
VIN1
Wire Wire Line
	10000 1500 9890 1500
Text Label 9890 1700 2    50   ~ 0
VIN2
Wire Wire Line
	10000 1700 9890 1700
Text Label 9890 1900 2    50   ~ 0
VIN3
Wire Wire Line
	10000 1900 9890 1900
Text Label 9890 2100 2    50   ~ 0
VIN4
Wire Wire Line
	10000 2100 9890 2100
Text Label 9890 2300 2    50   ~ 0
VIN5
Wire Wire Line
	10000 2300 9890 2300
Text Label 9890 2500 2    50   ~ 0
VIN6
Wire Wire Line
	10000 2500 9890 2500
Text Label 9890 2700 2    50   ~ 0
VIN7
Wire Wire Line
	10000 2700 9890 2700
Text Label 9890 2900 2    50   ~ 0
VIN8
Wire Wire Line
	10000 2900 9890 2900
Text Label 9890 3100 2    50   ~ 0
VIN9
Wire Wire Line
	10000 3100 9890 3100
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J11
U 1 1 608B6A38
P 1950 5200
F 0 "J11" H 2000 5417 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 2000 5326 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x02_P2.54mm_Vertical" H 1950 5200 50  0001 C CNN
F 3 "~" H 1950 5200 50  0001 C CNN
	1    1950 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 608B7BA2
P 1050 5200
F 0 "#PWR09" H 1050 4950 50  0001 C CNN
F 1 "GND" H 1055 5027 50  0000 C CNN
F 2 "" H 1050 5200 50  0001 C CNN
F 3 "" H 1050 5200 50  0001 C CNN
	1    1050 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 608B8210
P 2950 5200
F 0 "#PWR010" H 2950 4950 50  0001 C CNN
F 1 "GND" H 2955 5027 50  0000 C CNN
F 2 "" H 2950 5200 50  0001 C CNN
F 3 "" H 2950 5200 50  0001 C CNN
	1    2950 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 5200 2950 5200
Wire Wire Line
	1750 5200 1050 5200
$Comp
L power:+5V #PWR013
U 1 1 608BE2D5
P 2400 5500
F 0 "#PWR013" H 2400 5350 50  0001 C CNN
F 1 "+5V" H 2415 5673 50  0000 C CNN
F 2 "" H 2400 5500 50  0001 C CNN
F 3 "" H 2400 5500 50  0001 C CNN
	1    2400 5500
	-1   0    0    1   
$EndComp
$Comp
L power:-5V #PWR012
U 1 1 608BF520
P 1550 5500
F 0 "#PWR012" H 1550 5600 50  0001 C CNN
F 1 "-5V" H 1565 5673 50  0000 C CNN
F 2 "" H 1550 5500 50  0001 C CNN
F 3 "" H 1550 5500 50  0001 C CNN
	1    1550 5500
	-1   0    0    1   
$EndComp
Wire Wire Line
	1550 5500 1550 5300
Wire Wire Line
	1550 5300 1750 5300
Wire Wire Line
	2250 5300 2400 5300
Wire Wire Line
	2400 5300 2400 5500
$Comp
L noise:QWIIC J12
U 1 1 608E8C43
P 1900 7000
F 0 "J12" H 1858 7610 45  0000 C CNN
F 1 "QWIIC" H 1858 7526 45  0000 C CNN
F 2 "noise:1X04_1MM_RA" H 1900 7500 20  0001 C CNN
F 3 "" H 1900 7000 50  0001 C CNN
F 4 "CONN-13729" H 1858 7431 60  0000 C CNN "Field4"
	1    1900 7000
	1    0    0    -1  
$EndComp
Text HLabel 2200 6700 2    50   Output ~ 0
SCL
Text HLabel 2200 6800 2    50   Output ~ 0
SDA
$Comp
L power:+3V3 #PWR015
U 1 1 608EB804
P 2700 6900
F 0 "#PWR015" H 2700 6750 50  0001 C CNN
F 1 "+3V3" H 2715 7073 50  0000 C CNN
F 2 "" H 2700 6900 50  0001 C CNN
F 3 "" H 2700 6900 50  0001 C CNN
	1    2700 6900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR016
U 1 1 608EE54C
P 2500 7050
F 0 "#PWR016" H 2500 6800 50  0001 C CNN
F 1 "GND" H 2505 6877 50  0000 C CNN
F 2 "" H 2500 7050 50  0001 C CNN
F 3 "" H 2500 7050 50  0001 C CNN
	1    2500 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 6900 2700 6900
Wire Wire Line
	2000 7000 2500 7000
Wire Wire Line
	2500 7000 2500 7050
$Comp
L noise:ADP123 U1
U 1 1 6090D523
P 6250 2800
F 0 "U1" H 6250 3497 60  0000 C CNN
F 1 "ADP123" H 6250 3391 60  0000 C CNN
F 2 "Package_TO_SOT_SMD:TSOT-23-5" H 6000 2800 60  0001 C CNN
F 3 "" H 6000 2800 60  0001 C CNN
	1    6250 2800
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR02
U 1 1 6090E9DA
P 4700 2300
F 0 "#PWR02" H 4700 2150 50  0001 C CNN
F 1 "+5V" H 4715 2473 50  0000 C CNN
F 2 "" H 4700 2300 50  0001 C CNN
F 3 "" H 4700 2300 50  0001 C CNN
	1    4700 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 6090F7A2
P 4700 2700
F 0 "C1" H 4700 2800 50  0000 L CNN
F 1 "1uF" H 4700 2600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4738 2550 50  0001 C CNN
F 3 "~" H 4700 2700 50  0001 C CNN
	1    4700 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 60910371
P 7400 2500
F 0 "R1" V 7500 2450 50  0000 L CNN
F 1 "133k" V 7400 2400 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7330 2500 50  0001 C CNN
F 3 "~" H 7400 2500 50  0001 C CNN
	1    7400 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 60910E3B
P 7400 3050
F 0 "R2" V 7500 3000 50  0000 L CNN
F 1 "33.2k" V 7400 2950 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7330 3050 50  0001 C CNN
F 3 "~" H 7400 3050 50  0001 C CNN
	1    7400 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 60911BF7
P 8000 2900
F 0 "C2" H 8000 3000 50  0000 L CNN
F 1 "1uF" H 8000 2800 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8038 2750 50  0001 C CNN
F 3 "~" H 8000 2900 50  0001 C CNN
	1    8000 2900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 60912E6C
P 6250 3400
F 0 "#PWR04" H 6250 3150 50  0001 C CNN
F 1 "GND" H 6255 3227 50  0000 C CNN
F 2 "" H 6250 3400 50  0001 C CNN
F 3 "" H 6250 3400 50  0001 C CNN
	1    6250 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 2300 4700 2350
Wire Wire Line
	5750 2350 4700 2350
Connection ~ 4700 2350
Wire Wire Line
	4700 2350 4700 2550
Wire Wire Line
	4700 2850 4700 3200
Wire Wire Line
	4700 3200 6250 3200
Wire Wire Line
	6250 3200 6250 3400
Wire Wire Line
	6250 2900 6250 3200
Connection ~ 6250 3200
Wire Wire Line
	6750 2650 7400 2650
Wire Wire Line
	6750 2350 7400 2350
Wire Wire Line
	8000 2350 8000 2750
Connection ~ 7400 2350
Wire Wire Line
	7400 2350 8000 2350
Wire Wire Line
	7400 2900 7400 2650
Connection ~ 7400 2650
Wire Wire Line
	7400 3200 6250 3200
Wire Wire Line
	8000 3050 8000 3200
Wire Wire Line
	8000 3200 7400 3200
Connection ~ 7400 3200
$Comp
L power:+3V3 #PWR03
U 1 1 60926045
P 5400 2600
F 0 "#PWR03" H 5400 2450 50  0001 C CNN
F 1 "+3V3" H 5415 2773 50  0000 C CNN
F 2 "" H 5400 2600 50  0001 C CNN
F 3 "" H 5400 2600 50  0001 C CNN
	1    5400 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 2650 5400 2650
Wire Wire Line
	5400 2650 5400 2600
$Comp
L power:+2V5 #PWR01
U 1 1 60929415
P 8350 2250
F 0 "#PWR01" H 8350 2100 50  0001 C CNN
F 1 "+2V5" H 8365 2423 50  0000 C CNN
F 2 "" H 8350 2250 50  0001 C CNN
F 3 "" H 8350 2250 50  0001 C CNN
	1    8350 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 2250 8350 2350
Wire Wire Line
	8350 2350 8000 2350
Connection ~ 8000 2350
$Comp
L power:-5V #PWR08
U 1 1 6093ACFD
P 4950 4700
F 0 "#PWR08" H 4950 4800 50  0001 C CNN
F 1 "-5V" H 4965 4873 50  0000 C CNN
F 2 "" H 4950 4700 50  0001 C CNN
F 3 "" H 4950 4700 50  0001 C CNN
	1    4950 4700
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:ADP7182AUJZ-2.5 U2
U 1 1 6093D51E
P 6450 4650
F 0 "U2" H 6450 4375 50  0000 C CNN
F 1 "ADP7182AUJZ-2.5" H 6450 4284 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TSOT-23-5" H 6450 4250 50  0001 C CIN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADP7182.pdf" H 6450 4150 50  0001 C CNN
	1    6450 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 6094153A
P 4950 5100
F 0 "C3" H 4950 5200 50  0000 L CNN
F 1 "1uF" H 4950 5000 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4988 4950 50  0001 C CNN
F 3 "~" H 4950 5100 50  0001 C CNN
	1    4950 5100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 6094196F
P 7650 5200
F 0 "C4" H 7650 5300 50  0000 L CNN
F 1 "1uF" H 7650 5100 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7688 5050 50  0001 C CNN
F 3 "~" H 7650 5200 50  0001 C CNN
	1    7650 5200
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR06
U 1 1 609421E0
P 5550 4450
F 0 "#PWR06" H 5550 4300 50  0001 C CNN
F 1 "+3V3" H 5565 4623 50  0000 C CNN
F 2 "" H 5550 4450 50  0001 C CNN
F 3 "" H 5550 4450 50  0001 C CNN
	1    5550 4450
	1    0    0    -1  
$EndComp
$Comp
L power:-2V5 #PWR07
U 1 1 60942FFC
P 7650 4600
F 0 "#PWR07" H 7650 4700 50  0001 C CNN
F 1 "-2V5" H 7665 4773 50  0000 C CNN
F 2 "" H 7650 4600 50  0001 C CNN
F 3 "" H 7650 4600 50  0001 C CNN
	1    7650 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 4700 4950 4750
Wire Wire Line
	6050 4750 4950 4750
Connection ~ 4950 4750
Wire Wire Line
	4950 4750 4950 4950
Wire Wire Line
	5550 4450 5550 4550
Wire Wire Line
	5550 4550 6050 4550
Wire Wire Line
	6850 4750 7650 4750
Wire Wire Line
	7650 4750 7650 4600
Wire Wire Line
	7650 4750 7650 5050
Connection ~ 7650 4750
$Comp
L power:GND #PWR011
U 1 1 6094FFDC
P 4950 5350
F 0 "#PWR011" H 4950 5100 50  0001 C CNN
F 1 "GND" H 4955 5177 50  0000 C CNN
F 2 "" H 4950 5350 50  0001 C CNN
F 3 "" H 4950 5350 50  0001 C CNN
	1    4950 5350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 60950519
P 7650 5500
F 0 "#PWR014" H 7650 5250 50  0001 C CNN
F 1 "GND" H 7655 5327 50  0000 C CNN
F 2 "" H 7650 5500 50  0001 C CNN
F 3 "" H 7650 5500 50  0001 C CNN
	1    7650 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 5250 4950 5350
Wire Wire Line
	7650 5350 7650 5500
$Comp
L power:GND #PWR05
U 1 1 60956165
P 7000 4250
F 0 "#PWR05" H 7000 4000 50  0001 C CNN
F 1 "GND" H 7005 4077 50  0000 C CNN
F 2 "" H 7000 4250 50  0001 C CNN
F 3 "" H 7000 4250 50  0001 C CNN
	1    7000 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 4250 6450 4250
Wire Wire Line
	6450 4250 6450 4350
Wire Wire Line
	2200 6700 2000 6700
Wire Wire Line
	2200 6800 2000 6800
Wire Wire Line
	1100 1200 1250 1200
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J3
U 1 1 609914A4
P 1450 1850
F 0 "J3" H 1500 2067 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 1500 1976 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x02_P2.54mm_Vertical" H 1450 1850 50  0001 C CNN
F 3 "~" H 1450 1850 50  0001 C CNN
	1    1450 1850
	1    0    0    -1  
$EndComp
Text Label 1100 1850 2    50   ~ 0
VIN2
Wire Wire Line
	1250 1850 1750 1850
Wire Wire Line
	1250 1950 1750 1950
Wire Wire Line
	1100 1850 1250 1850
Connection ~ 1250 1850
Connection ~ 1250 1950
Wire Wire Line
	1100 1950 1250 1950
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J5
U 1 1 60994485
P 1450 2550
F 0 "J5" H 1500 2767 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 1500 2676 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x02_P2.54mm_Vertical" H 1450 2550 50  0001 C CNN
F 3 "~" H 1450 2550 50  0001 C CNN
	1    1450 2550
	1    0    0    -1  
$EndComp
Text Label 1100 2550 2    50   ~ 0
VIN4
Wire Wire Line
	1250 2550 1750 2550
Wire Wire Line
	1250 2650 1750 2650
Wire Wire Line
	1100 2550 1250 2550
Connection ~ 1250 2550
Connection ~ 1250 2650
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J7
U 1 1 60997771
P 1450 3200
F 0 "J7" H 1500 3417 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 1500 3326 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x02_P2.54mm_Vertical" H 1450 3200 50  0001 C CNN
F 3 "~" H 1450 3200 50  0001 C CNN
	1    1450 3200
	1    0    0    -1  
$EndComp
Text Label 1100 3200 2    50   ~ 0
VIN6
Wire Wire Line
	1250 3200 1750 3200
Wire Wire Line
	1250 3300 1750 3300
Wire Wire Line
	1100 3200 1250 3200
Connection ~ 1250 3200
Connection ~ 1250 3300
Wire Wire Line
	1100 3300 1250 3300
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J9
U 1 1 6099AC5A
P 1450 3900
F 0 "J9" H 1500 4117 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 1500 4026 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x02_P2.54mm_Vertical" H 1450 3900 50  0001 C CNN
F 3 "~" H 1450 3900 50  0001 C CNN
	1    1450 3900
	1    0    0    -1  
$EndComp
Text Label 1100 3900 2    50   ~ 0
VIN8
Wire Wire Line
	1250 3900 1750 3900
Wire Wire Line
	1250 4000 1750 4000
Wire Wire Line
	1100 3900 1250 3900
Connection ~ 1250 3900
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J2
U 1 1 609A577C
P 2950 1100
F 0 "J2" H 3000 1317 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 3000 1226 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x02_P2.54mm_Vertical" H 2950 1100 50  0001 C CNN
F 3 "~" H 2950 1100 50  0001 C CNN
	1    2950 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 1100 3250 1100
Wire Wire Line
	2750 1200 3250 1200
Connection ~ 2750 1100
Connection ~ 2750 1200
Text Label 2600 1200 2    50   ~ 0
VIN1
Wire Wire Line
	2600 1200 2750 1200
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J4
U 1 1 609AE8E7
P 2950 1900
F 0 "J4" H 3000 2117 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 3000 2026 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x02_P2.54mm_Vertical" H 2950 1900 50  0001 C CNN
F 3 "~" H 2950 1900 50  0001 C CNN
	1    2950 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 1900 3250 1900
Wire Wire Line
	2750 2000 3250 2000
Connection ~ 2750 1900
Connection ~ 2750 2000
Text Label 2600 2000 2    50   ~ 0
VIN3
Wire Wire Line
	2600 2000 2750 2000
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J6
U 1 1 609B12E6
P 2950 2550
F 0 "J6" H 3000 2767 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 3000 2676 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x02_P2.54mm_Vertical" H 2950 2550 50  0001 C CNN
F 3 "~" H 2950 2550 50  0001 C CNN
	1    2950 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 2550 3250 2550
Wire Wire Line
	2750 2650 3250 2650
Connection ~ 2750 2550
Connection ~ 2750 2650
Text Label 2600 2650 2    50   ~ 0
VIN5
Wire Wire Line
	2600 2650 2750 2650
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J8
U 1 1 609B48AE
P 2950 3200
F 0 "J8" H 3000 3417 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 3000 3326 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x02_P2.54mm_Vertical" H 2950 3200 50  0001 C CNN
F 3 "~" H 2950 3200 50  0001 C CNN
	1    2950 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 3200 3250 3200
Wire Wire Line
	2750 3300 3250 3300
Connection ~ 2750 3200
Connection ~ 2750 3300
Text Label 2600 3300 2    50   ~ 0
VIN7
Wire Wire Line
	2600 3300 2750 3300
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J10
U 1 1 609B8567
P 2950 3900
F 0 "J10" H 3000 4117 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 3000 4026 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x02_P2.54mm_Vertical" H 2950 3900 50  0001 C CNN
F 3 "~" H 2950 3900 50  0001 C CNN
	1    2950 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 3900 3250 3900
Wire Wire Line
	2750 4000 3250 4000
Connection ~ 2750 3900
Connection ~ 2750 4000
Text Label 2600 4000 2    50   ~ 0
VIN9
Wire Wire Line
	2600 4000 2750 4000
$Comp
L power:GND #PWR0101
U 1 1 60C67130
P 1150 4070
F 0 "#PWR0101" H 1150 3820 50  0001 C CNN
F 1 "GND" H 1155 3897 50  0000 C CNN
F 2 "" H 1150 4070 50  0001 C CNN
F 3 "" H 1150 4070 50  0001 C CNN
	1    1150 4070
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 4000 1150 4000
Wire Wire Line
	1150 4000 1150 4070
Connection ~ 1250 4000
$Comp
L power:GND #PWR0102
U 1 1 60C6BE7A
P 1100 3360
F 0 "#PWR0102" H 1100 3110 50  0001 C CNN
F 1 "GND" H 1105 3187 50  0000 C CNN
F 2 "" H 1100 3360 50  0001 C CNN
F 3 "" H 1100 3360 50  0001 C CNN
	1    1100 3360
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 60C6C323
P 1110 2720
F 0 "#PWR0103" H 1110 2470 50  0001 C CNN
F 1 "GND" H 1115 2547 50  0000 C CNN
F 2 "" H 1110 2720 50  0001 C CNN
F 3 "" H 1110 2720 50  0001 C CNN
	1    1110 2720
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 60C6CC8C
P 1100 2070
F 0 "#PWR0104" H 1100 1820 50  0001 C CNN
F 1 "GND" H 1105 1897 50  0000 C CNN
F 2 "" H 1100 2070 50  0001 C CNN
F 3 "" H 1100 2070 50  0001 C CNN
	1    1100 2070
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 60C6D471
P 1100 1370
F 0 "#PWR0105" H 1100 1120 50  0001 C CNN
F 1 "GND" H 1105 1197 50  0000 C CNN
F 2 "" H 1100 1370 50  0001 C CNN
F 3 "" H 1100 1370 50  0001 C CNN
	1    1100 1370
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 60C6DE7A
P 2370 1260
F 0 "#PWR0106" H 2370 1010 50  0001 C CNN
F 1 "GND" H 2375 1087 50  0000 C CNN
F 2 "" H 2370 1260 50  0001 C CNN
F 3 "" H 2370 1260 50  0001 C CNN
	1    2370 1260
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 60C6EA80
P 2380 2030
F 0 "#PWR0107" H 2380 1780 50  0001 C CNN
F 1 "GND" H 2385 1857 50  0000 C CNN
F 2 "" H 2380 2030 50  0001 C CNN
F 3 "" H 2380 2030 50  0001 C CNN
	1    2380 2030
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 60C6F387
P 2380 2660
F 0 "#PWR0108" H 2380 2410 50  0001 C CNN
F 1 "GND" H 2385 2487 50  0000 C CNN
F 2 "" H 2380 2660 50  0001 C CNN
F 3 "" H 2380 2660 50  0001 C CNN
	1    2380 2660
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 60C6FF7D
P 2380 3330
F 0 "#PWR0109" H 2380 3080 50  0001 C CNN
F 1 "GND" H 2385 3157 50  0000 C CNN
F 2 "" H 2380 3330 50  0001 C CNN
F 3 "" H 2380 3330 50  0001 C CNN
	1    2380 3330
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 60C70F95
P 2380 4080
F 0 "#PWR0110" H 2380 3830 50  0001 C CNN
F 1 "GND" H 2385 3907 50  0000 C CNN
F 2 "" H 2380 4080 50  0001 C CNN
F 3 "" H 2380 4080 50  0001 C CNN
	1    2380 4080
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 3360 1100 3300
Wire Wire Line
	1110 2720 1110 2650
Wire Wire Line
	1110 2650 1250 2650
Wire Wire Line
	1100 2070 1100 1950
Wire Wire Line
	1100 1370 1100 1200
Wire Wire Line
	2370 1260 2370 1100
Wire Wire Line
	2370 1100 2750 1100
Wire Wire Line
	2380 2030 2380 1900
Wire Wire Line
	2380 1900 2750 1900
Wire Wire Line
	2380 2660 2380 2550
Wire Wire Line
	2380 2550 2750 2550
Wire Wire Line
	2380 3330 2380 3200
Wire Wire Line
	2380 3200 2750 3200
Wire Wire Line
	2380 3900 2750 3900
Wire Wire Line
	2380 3900 2380 4080
Wire Bus Line
	10100 1250 10100 3370
$EndSCHEMATC
