import ROOT as rt
import numpy as np


rt.gROOT.SetBatch(1)

Freq = []
RMS = []

lines = open("summary_tri_vs_freq.txt").readlines()
baseRMS = 1.0

for line in lines:
    line_arr = line.split(",")
    Freq.append(float(line_arr[0]))
    RMS.append(float(line_arr[1])/50.0)
    if float(line_arr[0]) == 2.0:
        baseRMS = float(line_arr[1])/50.0

for idx in range(len(RMS)):
    RMS[idx] = RMS[idx]/baseRMS

minY = np.array(RMS).min()
maxY = np.array(RMS).max()

myC = rt.TCanvas("myC","myC", 800, 600)
myC.SetBottomMargin(0.12)
myC.SetLeftMargin(0.12)
myC.SetLogx(1)
myC.SetGridx(1)
myC.SetGridy(1)

gr = rt.TGraph(len(Freq), np.array(Freq), np.array(RMS))

gr.SetTitle("")
gr.GetXaxis().SetTitle("Frequency f (MHz)")
gr.GetYaxis().SetTitle("Response (RMS_{f}/RMS_{2MHz})")

gr.GetYaxis().SetTitleSize(0.065)
gr.GetYaxis().SetTitleOffset(0.75)
gr.GetYaxis().SetLabelSize(0.055)
gr.GetYaxis().SetRangeUser(0.8*minY, 1.2*maxY)

gr.GetXaxis().SetTitleSize(0.055)
gr.GetXaxis().SetTitleOffset(1.1)
gr.GetXaxis().SetLabelSize(0.055)

gr.SetLineColor(rt.kBlue)
gr.SetMarkerColor(rt.kBlue)
gr.SetMarkerStyle(8)
gr.SetLineWidth(2)
gr.Draw("ALP")

myC.SaveAs("RMS_vs_freq_tri.pdf")


