import ROOT as rt
import numpy as np


rt.gROOT.SetBatch(1)

VIN_100_10 = []
RMS_100_10 = []
VIN_50_0p1 = []
RMS_50_0p1 = []
VIN_100_0p1 = []
RMS_100_0p1 = []

lines_100_10 = open("summary_tri_100_10uF.txt").readlines()
lines_50_0p1 = open("summary_tri_50_0.1uF.txt").readlines()
lines_100_0p1 = open("summary_tri_100_0.1uF.txt").readlines()

for line in lines_100_10:
    line_arr = line.split(",")
    VIN_100_10.append(float(line_arr[0]))
    RMS_100_10.append(float(line_arr[1])/100.0)

for line in lines_50_0p1:
    line_arr = line.split(",")
    VIN_50_0p1.append(float(line_arr[0]))
    RMS_50_0p1.append(float(line_arr[1])/50.0)

for line in lines_100_0p1:
    line_arr = line.split(",")
    VIN_100_0p1.append(float(line_arr[0]))
    RMS_100_0p1.append(float(line_arr[1])/100.0)

maxY = np.array([RMS_100_10[-1], RMS_50_0p1[-1], RMS_100_0p1[-1]]).max()
print(maxY)

myC = rt.TCanvas("myC","myC", 800, 600)
myC.SetBottomMargin(0.12)

gr_100_10 = rt.TGraph(len(VIN_100_10), np.array(VIN_100_10), np.array(RMS_100_10))
gr_50_0p1 = rt.TGraph(len(VIN_50_0p1), np.array(VIN_50_0p1), np.array(RMS_50_0p1))
gr_100_0p1 = rt.TGraph(len(VIN_100_0p1), np.array(VIN_100_0p1), np.array(RMS_100_0p1))

gr_100_10.SetTitle("")
gr_100_10.GetXaxis().SetTitle("Vp (mV)")
gr_100_10.GetYaxis().SetTitle("Measured RMS (mV)")

gr_100_10.GetYaxis().SetTitleSize(0.055)
gr_100_10.GetYaxis().SetTitleOffset(0.75)
gr_100_10.GetYaxis().SetLabelSize(0.055)
gr_100_10.GetYaxis().SetRangeUser(0, 1.2*maxY)

gr_100_10.GetXaxis().SetTitleSize(0.055)
gr_100_10.GetXaxis().SetTitleOffset(0.9)
gr_100_10.GetXaxis().SetLabelSize(0.055)

gr_100_10.SetLineColor(rt.kBlue)
gr_100_10.SetMarkerColor(rt.kBlue)
gr_100_10.SetMarkerStyle(22)
gr_100_10.SetLineWidth(2)
gr_100_10.Draw("AP")

gr_100_0p1.SetLineColor(rt.kGreen)
gr_100_0p1.SetMarkerColor(rt.kGreen)
gr_100_0p1.SetMarkerStyle(21)
gr_100_0p1.SetLineWidth(2)
gr_100_0p1.Draw("Psame")


gr_50_0p1.SetLineColor(rt.kRed)
gr_50_0p1.SetMarkerColor(rt.kRed)
gr_50_0p1.SetMarkerStyle(8)
gr_50_0p1.SetLineWidth(2)
gr_50_0p1.Draw("Psame")


f_100_10 = rt.TF1("f_100_10", "[0] + [1]*x", 0.0, 50.0)
f_100_10.SetLineColor(rt.kBlue)
gr_100_10.Fit(f_100_10, "", "", 1., 20.)
#f_100_10.Draw("same")

f_50_0p1 = rt.TF1("f_50_0p1", "[0] + [1]*x", 0.0, 50.0)
f_50_0p1.SetLineColor(rt.kRed)
gr_50_0p1.Fit(f_50_0p1, "", "", 1., 35.0)
f_50_0p1.Draw("same")

f_100_0p1 = rt.TF1("f_100_0p1", "[0] + [1]*x", 0.0, 50.0)
f_100_0p1.SetLineColor(rt.kGreen)
gr_100_0p1.Fit(f_100_0p1, "", "", 1., 20.)
#f_100_0p1.Draw("same")

leg = rt.TLegend(0.15, 0.70, 0.4, 0.88)
leg.SetFillStyle(0)
leg.SetBorderSize(0)
leg.SetTextFont(42)
leg.SetTextSize(0.03)

leg.AddEntry(gr_100_10, "gain 100, 10 uF, RMS = %.3f"%f_100_10.GetParameter(0)+" + %.3f"%f_100_10.GetParameter(1)+"Vp", "lp")
leg.AddEntry(gr_100_0p1, "gain 100, 0.1 uF, RMS = %.3f"%f_100_0p1.GetParameter(0)+" + %.3f"%f_100_0p1.GetParameter(1)+"Vp", "lp")
leg.AddEntry(gr_50_0p1, "gain 50, 0.1 uF, RMS = %.3f"%f_50_0p1.GetParameter(0)+" + %.3f"%f_50_0p1.GetParameter(1)+"Vp", "lp")
leg.Draw()


myC.SaveAs("RMS_vs_Vin_tri_all.pdf")


