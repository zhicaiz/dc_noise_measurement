import ROOT as rt
import numpy as np


rt.gROOT.SetBatch(1)
rt.gStyle.SetOptStat(0)
rt.gStyle.SetOptFit(111)

RMS_hist = rt.TH1F("RMS_hist", "", 30, 4.0, 4.6)

lines_hist = open("base_hist.txt").readlines()

for line in lines_hist:
    RMS_hist.Fill(float(line)/50.0)


myC = rt.TCanvas("myC","myC", 800, 600)
myC.SetBottomMargin(0.12)

RMS_hist.SetTitle("")
RMS_hist.GetXaxis().SetTitle("Measured RMS (mV)")
RMS_hist.GetYaxis().SetTitle("Events")

RMS_hist.GetYaxis().SetTitleSize(0.055)
RMS_hist.GetYaxis().SetTitleOffset(0.75)
RMS_hist.GetYaxis().SetLabelSize(0.055)
#RMS_hist.GetYaxis().SetRangeUser(0, 1.2*maxY)

RMS_hist.GetXaxis().SetTitleSize(0.055)
RMS_hist.GetXaxis().SetTitleOffset(0.9)
RMS_hist.GetXaxis().SetLabelSize(0.055)

RMS_hist.SetLineColor(rt.kBlue)
RMS_hist.SetMarkerColor(rt.kBlue)
RMS_hist.SetMarkerStyle(8)
RMS_hist.SetLineWidth(2)
RMS_hist.Draw("hist")

f1 = rt.TF1("f1", "gaus", 4.2, 4.35)
RMS_hist.Fit(f1,"","", 4.2,4.35)
f1.Draw("same")

myC.SaveAs("base_hist.pdf")


