import ROOT as rt
import numpy as np


rt.gROOT.SetBatch(1)

VIN_50_2 = []
RMS_50_2 = []

lines_50_2 = open("summary_base_tri_50_2uF.txt").readlines()

for line in lines_50_2:
    line_arr = line.split(",")
    VIN_50_2.append(float(line_arr[0]))
    RMS_50_2.append(float(line_arr[1])/50.0)

maxY = np.array([RMS_50_2[-1]]).max()
print(maxY)

myC = rt.TCanvas("myC","myC", 800, 600)
myC.SetBottomMargin(0.12)

gr_50_2 = rt.TGraph(len(VIN_50_2), np.array(VIN_50_2), np.array(RMS_50_2))

gr_50_2.SetTitle("")
gr_50_2.GetXaxis().SetTitle("Injected Triangle Vp (mV)")
gr_50_2.GetYaxis().SetTitle("Measured RMS (mV)")

gr_50_2.GetYaxis().SetTitleSize(0.055)
gr_50_2.GetYaxis().SetTitleOffset(0.75)
gr_50_2.GetYaxis().SetLabelSize(0.055)
gr_50_2.GetYaxis().SetRangeUser(0, 1.2*maxY)

gr_50_2.GetXaxis().SetTitleSize(0.055)
gr_50_2.GetXaxis().SetTitleOffset(0.9)
gr_50_2.GetXaxis().SetLabelSize(0.055)

gr_50_2.SetLineColor(rt.kBlue)
gr_50_2.SetMarkerColor(rt.kBlue)
gr_50_2.SetMarkerStyle(8)
gr_50_2.SetLineWidth(2)
gr_50_2.Draw("AP")


#f_50_2 = rt.TF1("f_50_2", "[0] + [1]*x", 0, VIN_50_2[-1])
f_50_2 = rt.TF1("f_50_2", "sqrt([0] + [1]*x*x)", 0, VIN_50_2[-1])
f_50_2.SetParameter(0, RMS_50_2[0]*RMS_50_2[0])
f_50_2.SetParameter(1, 0.25)
f_50_2.SetLineColor(rt.kBlue)
gr_50_2.Fit(f_50_2, "", "", 0., 36.0)
f_50_2.Draw("same")

leg = rt.TLegend(0.15, 0.70, 0.4, 0.88)
leg.SetFillStyle(0)
leg.SetBorderSize(0)
leg.SetTextFont(42)
leg.SetTextSize(0.03)

#leg.AddEntry(gr_50_2, "RMS = %.3f"%f_50_2.GetParameter(0)+" + %.3f"%f_50_2.GetParameter(1)+"Vp", "lp")
leg.AddEntry(gr_50_2, "RMS = #sqrt{%.3f"%np.sqrt(f_50_2.GetParameter(0))+"^{2} + (%.3f"%np.sqrt(f_50_2.GetParameter(1))+"Vp)^{2}}", "lp")
leg.Draw()

myC.SaveAs("RMS_vs_Vin_base_tri_all.pdf")


