EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title "DC Noise Measurement Board"
Date "2021-04-28"
Rev "v1"
Comp "LBNL"
Comment1 "Zhicai Zhang"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1950 2500 850  1050
U 60988A05
F0 "Connectors" 50
F1 "Connectors.sch" 50
F2 "VIN[0..9]" O R 2800 2700 50 
F3 "SCL" O R 2800 3000 50 
F4 "SDA" O R 2800 3150 50 
$EndSheet
$Sheet
S 4200 2500 950  1100
U 60988A7C
F0 "Mux" 50
F1 "Mux.sch" 50
F2 "VIN[0..9]" I L 4200 2700 50 
F3 "VIN" O R 5150 2700 50 
F4 "SCL" I L 4200 3000 50 
F5 "SDA" I L 4200 3150 50 
$EndSheet
$Sheet
S 6450 2500 850  1100
U 6084D42A
F0 "RMSToDC" 50
F1 "RMSToDC.sch" 50
F2 "RMSout" O R 7300 2700 50 
F3 "VIN" I L 6450 2700 50 
$EndSheet
$Sheet
S 8450 2500 700  1100
U 60988BBA
F0 "ADC" 50
F1 "ADC.sch" 50
F2 "RMSout" I L 8450 2700 50 
F3 "SCL" I L 8450 2850 50 
F4 "SDA" I L 8450 3000 50 
$EndSheet
Text Label 3000 2700 0    50   ~ 0
VIN[0..9]
Text Label 3000 3000 0    50   ~ 0
SCL
Text Label 3000 3150 0    50   ~ 0
SDA
Wire Bus Line
	3000 2700 2800 2700
Wire Wire Line
	3000 3000 2800 3000
Wire Wire Line
	3000 3150 2800 3150
Text Label 4000 2700 2    50   ~ 0
VIN[0..9]
Text Label 4000 3000 2    50   ~ 0
SCL
Text Label 4000 3150 2    50   ~ 0
SDA
Wire Bus Line
	4000 2700 4200 2700
Wire Wire Line
	4000 3000 4200 3000
Wire Wire Line
	4000 3150 4200 3150
Text Label 5350 2700 0    50   ~ 0
VIN
Text Label 6250 2700 2    50   ~ 0
VIN
Wire Wire Line
	5350 2700 5150 2700
Wire Wire Line
	6450 2700 6250 2700
Text Label 7450 2700 0    50   ~ 0
RMSout
Text Label 8250 2700 2    50   ~ 0
RMSout
Text Label 8250 2850 2    50   ~ 0
SCL
Text Label 8250 3000 2    50   ~ 0
SDA
Wire Wire Line
	7450 2700 7300 2700
Wire Wire Line
	8450 2700 8250 2700
Wire Wire Line
	8450 2850 8250 2850
Wire Wire Line
	8450 3000 8250 3000
$EndSCHEMATC
